from typing import List
from town_generator import City
import numpy as np
from geopy.distance import geodesic


def get_distances(lst_of_cities: List[City]) -> np.ndarray:
    result = (np.ndarray(shape=(len(lst_of_cities), len(lst_of_cities))))
    for i in range(len(lst_of_cities)):
        for j in range(len(lst_of_cities)):
            # np.append(arr = result, values = (get_distance_between_two_cities(lst_of_cities[i], lst_of_cities[j])))
            result[i][j] = (get_distance_between_two_cities(lst_of_cities[i], lst_of_cities[j]))

    return result

def get_distance_between_two_cities(city1: City, city2: City) -> float:
    coord1 = (str(city1.latitude), str(city1.longitude))
    coord2 = (str(city2.latitude), str(city2.longitude))

    distance = geodesic(coord1, coord2).kilometers
    print(distance)

    return distance

