from town_generator import generate_cities
from distances import get_distances
from route_planning import plan_route
from visualization import visualize
import pickle


def generate_task():
    cities_10 = generate_cities(10)
    cities_25 = generate_cities(25)
    cities_100 = generate_cities(100)
    with open('cities_10.pickle', 'wb') as handle:
        pickle.dump(cities_10, handle, protocol=pickle.HIGHEST_PROTOCOL)

    with open('cities_25.pickle', 'wb') as handle:
        pickle.dump(cities_25, handle, protocol=pickle.HIGHEST_PROTOCOL)

    with open('cities_100.pickle', 'wb') as handle:
        pickle.dump(cities_100, handle, protocol=pickle.HIGHEST_PROTOCOL)


def main():
    with open('cities_10.pickle', 'rb') as handle:
        cities_10 = pickle.load(handle)

    with open('cities_25.pickle', 'rb') as handle:
        cities_25 = pickle.load(handle)

    with open('cities_100.pickle', 'rb') as handle:
        cities_100 = pickle.load(handle)

    # Example workflow
    distances = get_distances(cities_10)
    # route = plan_route(distances)
    # visualize(route)

    print(distances)

    # TODO: Task 1 Cities_10
    # TODO: Task 2 Cities_25
    # TODO: Task 3 Cities_100


if __name__ == '__main__':
    main()
