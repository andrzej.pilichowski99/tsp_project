import numpy as np
from typing import List
from town_generator import City


def plan_route(lst: List[City], distances: np.ndarray) -> List[City]:
    return lst

def get_route_distance(lst: List[City]) -> float:
    pass

def distance_between_two_cities(index_city1, index_city2, distances) -> float:
    return distances[index_city1, index_city2]
